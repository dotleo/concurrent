package cn.com.dotleo.summarize;

/**
 * Created by liufei on 2018/6/16.
 */
public class VolatileDemo {

    private volatile int num = 0;

    public int getNum() {
        return this.num;
    }

    public void increase() {
        this.num++;
    }

    public static void main(String[] args) {
        final VolatileDemo volatileDemo = new VolatileDemo();
        for (int i = 0; i < 500; i++) {
            new Thread(new Runnable() {
                public void run() {
                    volatileDemo.increase();
                }
            }).start();
        }

        // 为了让所有线程执行完毕
        // 如果还有子线程执行
        // 主线程让出cpu资源
        while (Thread.activeCount() > 1) {
            Thread.yield();
        }
        System.out.println("num" + volatileDemo.getNum());
    }
}
