package cn.com.dotleo.sync;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by liufei on 2018/6/24.
 */
public class Sync1 {

    public void test1(int x) {
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                System.out.println("["+ x + "]test1: " + i);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void main(String[] args) {
//        final Sync1 sync = new Sync1();
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        executorService.execute(new Runnable() {
//            public void run() {
//                sync.test1(1);
//            }
//        });
//
//        executorService.execute(new Runnable() {
//            public void run() {
//                sync.test1(2);
//            }
//        });
        final Sync1 sync1 = new Sync1();
        final Sync1 syncNew = new Sync1();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new Runnable() {
            public void run() {
                sync1.test1(1);
            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                syncNew.test1(2);
            }
        });


    }
}
