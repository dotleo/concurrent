package cn.com.dotleo.sync;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by liufei on 2018/6/24.
 */
public class Sync4 {

    public static void test4(int x) {
        synchronized (Sync4.class) {
            for (int i = 0; i < 10; i++) {
                System.out.println("["+ x + "]test4: " + i);
            }
        }

    }

    public static void main(String[] args) {

        final Sync4 sync4 = new Sync4();
        final Sync4 syncNew = new Sync4();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new Runnable() {
            public void run() {
                sync4.test4(1);
            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                syncNew.test4(2);
            }
        });
    }
}
