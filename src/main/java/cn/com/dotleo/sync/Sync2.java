package cn.com.dotleo.sync;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by liufei on 2018/6/24.
 */
public class Sync2 {

    public synchronized void test2(int x) {
        for (int i = 0; i < 10; i++) {
            System.out.println("["+ x + "]test2: " + i);
        }
    }

    public static void main(String[] args) {
//        final Sync2 sync = new Sync2();
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        executorService.execute(new Runnable() {
//            public void run() {
//                sync.test2(1);
//            }
//        });
//
//        executorService.execute(new Runnable() {
//            public void run() {
//                sync.test2(2);
//            }
//        });
        final Sync2 sync2 = new Sync2();
        final Sync2 syncNew = new Sync2();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new Runnable() {
            public void run() {
                sync2.test2(1);
            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                syncNew.test2(2);
            }
        });
    }
}
