package cn.com.dotleo.sync;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by liufei on 2018/6/24.
 */
public class Sync3 {

    public static synchronized void test3(int x) {
        for (int i = 0; i < 10; i++) {
            System.out.println("["+ x + "]test3: " + i);
        }
    }

    public static void main(String[] args) {

        final Sync3 sync3 = new Sync3();
        final Sync3 syncNew = new Sync3();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new Runnable() {
            public void run() {
                sync3.test3(1);
            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                syncNew.test3(2);
            }
        });
    }
}
